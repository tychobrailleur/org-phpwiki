class MimeHeaderParser

%%{

    machine mime_header_parser;

    any_char = [a-zA-Z0-9,\.\- :\+\(\);/"_=%] ;
    header_name_char = any_char - ':' ;
    eol = '\r\n' | '\r' | '\n' ;

    action mark { mark = p }
    action start_string { @start_string = p }
    action append_header {
        if current_value != ''
           metadata[current_header] = current_value
        end
        current_header = data[@start_string...p].pack("c*")
    }

    action append_value {
        current_value = data[@start_string...p].pack("c*")
    }
    action concat_value {
        current_value += data[@start_string...p].pack("c*")
    }
    action push_last_header {
        metadata[current_header] = current_value
    }
    action set_metadata_multipart_number {
        metadata['multipart_number'] = data[mark..p].pack("c*")
    }


    header_name = header_name_char+ ;
    header_value = (any_char)+ >start_string %append_value ;
    header_continuation = ' ' ' '  (any_char)+ %concat_value ;
    header_line = (header_name >start_string %append_header
                   ':' ' '? header_value eol | header_continuation eol );

    main := header_line+ eol eol %push_last_header
            any+ ; # Ignore the rest of the file.


}%%

    def initialize
      %% write data;
    end

    def scan(data)
      # convert string into an array of 8-bit signed integers.
      data = data.unpack("c*") if data.is_a?(String)
      # initialize the end of the string
      eof = pe = data.length
      current_header = ''
      current_value = ''
      metadata = Hash.new
      metadata['multiparts'] = []
      current_multipart_meta = nil
      mark = -1

      %% write init;
      %% write exec;

      metadata
    end

end
