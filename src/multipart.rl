class MultipartParser

%%{

    machine multipart_parser;

    any_char = [a-zA-Z0-9,\.\- :\+\(\);/"_=] ;
    header_name_char = any_char - ':' ;
    eol = '\r\n' | '\r' | '\n' ;

    action mark { mark = p }
    action start_string { @start_string = p }

    action append_multipart_header {
        if current_value != ''
           current_multipart_meta[current_header] = current_value
        end
        current_header = data[@start_string...p].pack("c*")
    }
    action append_value {
        current_value = data[@start_string...p].pack("c*")
    }
    action set_metadata_multipart_number {
        metadata['multipart_number'] = data[mark..p].pack("c*")
    }
    action multipart_body_value {
    puts "Pouet"
        current_multipart_meta['body'] = data[mark..p].pack("c*")
        puts current_multipart_meta['body']
    }
    action init_multipart {
        if current_multipart_meta != nil
           metadata['multiparts'] << current_multipart_meta
        end
        current_multipart_meta = Hash.new
    }

    header_name = header_name_char+ ;
    header_value = (any_char)+  ;
    header_continuation = ' ' ' '  (any_char)+  ;
    header_line = (header_name  ':' ' '? header_value eol | header_continuation eol );

    multipart_number = [0-9]+ >mark @set_metadata_multipart_number ;
    multipart_boundary = '--=_multipart_boundary_' multipart_number eol ;
    multipart_boundary_end = '--=_multipart_boundary_' multipart_number '--' eol ;

    multipart_header = (header_name >start_string %append_multipart_header
                        ':' ' '? header_value eol | header_continuation eol );
    multipart_body = any+ >mark %multipart_body_value ;

    multipart = multipart_header+ eol eol
                multipart_body ;

    main := header_line+ eol eol
            (multipart_boundary %init_multipart multipart)+ multipart_boundary_end ;
}%%

    def initialize
      %% write data;
    end

    def scan(data)
      # convert string into an array of 8-bit signed integers.
      data = data.unpack("c*") if data.is_a?(String)
      # initialize the end of the string
      eof = pe = data.length
      current_header = ''
      current_value = ''
      metadata = Hash.new
      metadata['multiparts'] = []
      current_multipart_meta = nil
      mark = -1

      %% write init;
      %% write exec;

      metada
    end

end
