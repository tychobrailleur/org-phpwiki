$:.unshift File.dirname(__FILE__)


require 'mail'
require 'fileutils'
require 'open3'

module Phpwiki2org

  OUTPUT_DIR = '/tmp/wikiorg'

  class Phpwiki2org
    def process(zipfile)
      entries = `zipinfo -1 #{zipfile}`
      entries.split(/\n/)
        .reject { |e| e.include?('Category') }
        .reject { |e| e.include?('Pgsrc') }
        .reject { |e| e.include?('AllPages') }
        .reject { |e| e.include?('PhpWiki') }
        .reject { |e| e.include?('Help') }.each do |entry|
        puts "Writing #{entry}..."

        output_path = "#{OUTPUT_DIR}/#{entry}"
        dirname = File.dirname(output_path)
        FileUtils.mkdir_p(dirname) unless File.directory?(dirname)

        output = `unzip -qq -c #{zipfile} \"#{entry}\" > \"#{OUTPUT_DIR}/#{entry}.txt\"`
        parsed_mime = Mail.read_from_string(File.read("#{OUTPUT_DIR}/#{entry}.txt"))

        ## pandoc -f tikiwiki -t org
#        puts parsed_mime

        if parsed_mime.multipart?
          puts parsed_mime.parts.first.body
        else
          puts parsed_mime.body
        end
      end
    end
  end
end
