
# line 1 "src/mime_header.rl"
class MimeHeaderParser


# line 44 "src/mime_header.rl"


    def initialize
      
# line 12 "lib/mime_header.rb"
class << self
	attr_accessor :_mime_header_parser_actions
	private :_mime_header_parser_actions, :_mime_header_parser_actions=
end
self._mime_header_parser_actions = [
	0, 1, 0, 1, 1, 1, 2, 1, 
	3, 1, 4, 2, 2, 3
]

class << self
	attr_accessor :_mime_header_parser_key_offsets
	private :_mime_header_parser_key_offsets, :_mime_header_parser_key_offsets=
end
self._mime_header_parser_key_offsets = [
	0, 0, 14, 28, 42, 58, 74, 76, 
	76, 77, 79, 81, 95, 108, 123, 139, 
	155, 157, 172, 187, 202, 217, 217, 217, 
	218
]

class << self
	attr_accessor :_mime_header_parser_trans_keys
	private :_mime_header_parser_trans_keys, :_mime_header_parser_trans_keys=
end
self._mime_header_parser_trans_keys = [
	32, 34, 37, 59, 61, 95, 40, 41, 
	43, 57, 65, 90, 97, 122, 32, 34, 
	37, 58, 61, 95, 40, 41, 43, 59, 
	65, 90, 97, 122, 32, 34, 37, 58, 
	61, 95, 40, 41, 43, 59, 65, 90, 
	97, 122, 10, 13, 32, 34, 37, 58, 
	61, 95, 40, 41, 43, 59, 65, 90, 
	97, 122, 10, 13, 32, 34, 37, 59, 
	61, 95, 40, 41, 43, 57, 65, 90, 
	97, 122, 10, 13, 10, 10, 13, 10, 
	13, 32, 34, 37, 58, 61, 95, 40, 
	41, 43, 59, 65, 90, 97, 122, 32, 
	34, 37, 61, 95, 40, 41, 43, 59, 
	65, 90, 97, 122, 10, 13, 32, 34, 
	37, 61, 95, 40, 41, 43, 59, 65, 
	90, 97, 122, 10, 13, 32, 34, 37, 
	59, 61, 95, 40, 41, 43, 57, 65, 
	90, 97, 122, 10, 13, 32, 34, 37, 
	59, 61, 95, 40, 41, 43, 57, 65, 
	90, 97, 122, 10, 13, 10, 13, 32, 
	34, 37, 61, 95, 40, 41, 43, 59, 
	65, 90, 97, 122, 10, 13, 32, 34, 
	37, 61, 95, 40, 41, 43, 59, 65, 
	90, 97, 122, 10, 13, 32, 34, 37, 
	61, 95, 40, 41, 43, 59, 65, 90, 
	97, 122, 10, 13, 32, 34, 37, 61, 
	95, 40, 41, 43, 59, 65, 90, 97, 
	122, 10, 10, 13, 0
]

class << self
	attr_accessor :_mime_header_parser_single_lengths
	private :_mime_header_parser_single_lengths, :_mime_header_parser_single_lengths=
end
self._mime_header_parser_single_lengths = [
	0, 6, 6, 6, 8, 8, 2, 0, 
	1, 2, 2, 6, 5, 7, 8, 8, 
	2, 7, 7, 7, 7, 0, 0, 1, 
	2
]

class << self
	attr_accessor :_mime_header_parser_range_lengths
	private :_mime_header_parser_range_lengths, :_mime_header_parser_range_lengths=
end
self._mime_header_parser_range_lengths = [
	0, 4, 4, 4, 4, 4, 0, 0, 
	0, 0, 0, 4, 4, 4, 4, 4, 
	0, 4, 4, 4, 4, 0, 0, 0, 
	0
]

class << self
	attr_accessor :_mime_header_parser_index_offsets
	private :_mime_header_parser_index_offsets, :_mime_header_parser_index_offsets=
end
self._mime_header_parser_index_offsets = [
	0, 0, 11, 22, 33, 46, 59, 62, 
	63, 65, 68, 71, 82, 92, 104, 117, 
	130, 133, 145, 157, 169, 181, 182, 183, 
	185
]

class << self
	attr_accessor :_mime_header_parser_indicies
	private :_mime_header_parser_indicies, :_mime_header_parser_indicies=
end
self._mime_header_parser_indicies = [
	0, 2, 2, 2, 2, 2, 2, 2, 
	2, 2, 1, 3, 4, 4, 5, 4, 
	4, 4, 4, 4, 4, 1, 6, 6, 
	6, 7, 6, 6, 6, 6, 6, 6, 
	1, 8, 9, 6, 6, 6, 7, 6, 
	6, 6, 6, 6, 6, 1, 10, 11, 
	0, 2, 2, 2, 2, 2, 2, 2, 
	2, 2, 1, 12, 13, 1, 14, 15, 
	14, 16, 13, 1, 15, 17, 14, 4, 
	4, 4, 5, 4, 4, 4, 4, 4, 
	4, 1, 18, 19, 19, 19, 19, 19, 
	19, 19, 19, 1, 20, 21, 19, 19, 
	19, 19, 19, 19, 19, 19, 19, 1, 
	22, 11, 0, 2, 2, 2, 2, 2, 
	2, 2, 2, 2, 1, 16, 23, 0, 
	2, 2, 2, 2, 2, 2, 2, 2, 
	2, 1, 24, 17, 14, 20, 21, 25, 
	25, 25, 25, 25, 25, 25, 25, 25, 
	1, 8, 9, 26, 27, 27, 27, 27, 
	27, 27, 27, 27, 1, 28, 29, 27, 
	27, 27, 27, 27, 27, 27, 27, 27, 
	1, 28, 29, 30, 30, 30, 30, 30, 
	30, 30, 30, 30, 1, 31, 14, 15, 
	14, 15, 17, 14, 0
]

class << self
	attr_accessor :_mime_header_parser_trans_targs
	private :_mime_header_parser_trans_targs, :_mime_header_parser_trans_targs=
end
self._mime_header_parser_trans_targs = [
	2, 0, 11, 3, 11, 12, 4, 18, 
	5, 14, 6, 9, 7, 8, 21, 22, 
	10, 23, 13, 17, 5, 14, 15, 16, 
	24, 17, 19, 20, 5, 14, 20, 21
]

class << self
	attr_accessor :_mime_header_parser_trans_actions
	private :_mime_header_parser_trans_actions, :_mime_header_parser_trans_actions=
end
self._mime_header_parser_trans_actions = [
	1, 0, 1, 0, 0, 3, 0, 3, 
	7, 7, 0, 0, 0, 0, 9, 9, 
	0, 9, 1, 1, 5, 5, 0, 0, 
	9, 0, 1, 1, 11, 11, 0, 0
]

class << self
	attr_accessor :mime_header_parser_start
end
self.mime_header_parser_start = 1;
class << self
	attr_accessor :mime_header_parser_first_final
end
self.mime_header_parser_first_final = 21;
class << self
	attr_accessor :mime_header_parser_error
end
self.mime_header_parser_error = 0;

class << self
	attr_accessor :mime_header_parser_en_main
end
self.mime_header_parser_en_main = 1;


# line 48 "src/mime_header.rl"
    end

    def scan(data)
      # convert string into an array of 8-bit signed integers.
      data = data.unpack("c*") if data.is_a?(String)
      # initialize the end of the string
      eof = pe = data.length
      current_header = ''
      current_value = ''
      metadata = Hash.new
      metadata['multiparts'] = []
      current_multipart_meta = nil
      mark = -1

      
# line 189 "lib/mime_header.rb"
begin
	p ||= 0
	pe ||= data.length
	cs = mime_header_parser_start
end

# line 63 "src/mime_header.rl"
      
# line 198 "lib/mime_header.rb"
begin
	_klen, _trans, _keys, _acts, _nacts = nil
	_goto_level = 0
	_resume = 10
	_eof_trans = 15
	_again = 20
	_test_eof = 30
	_out = 40
	while true
	_trigger_goto = false
	if _goto_level <= 0
	if p == pe
		_goto_level = _test_eof
		next
	end
	if cs == 0
		_goto_level = _out
		next
	end
	end
	if _goto_level <= _resume
	_keys = _mime_header_parser_key_offsets[cs]
	_trans = _mime_header_parser_index_offsets[cs]
	_klen = _mime_header_parser_single_lengths[cs]
	_break_match = false
	
	begin
	  if _klen > 0
	     _lower = _keys
	     _upper = _keys + _klen - 1

	     loop do
	        break if _upper < _lower
	        _mid = _lower + ( (_upper - _lower) >> 1 )

	        if data[p].ord < _mime_header_parser_trans_keys[_mid]
	           _upper = _mid - 1
	        elsif data[p].ord > _mime_header_parser_trans_keys[_mid]
	           _lower = _mid + 1
	        else
	           _trans += (_mid - _keys)
	           _break_match = true
	           break
	        end
	     end # loop
	     break if _break_match
	     _keys += _klen
	     _trans += _klen
	  end
	  _klen = _mime_header_parser_range_lengths[cs]
	  if _klen > 0
	     _lower = _keys
	     _upper = _keys + (_klen << 1) - 2
	     loop do
	        break if _upper < _lower
	        _mid = _lower + (((_upper-_lower) >> 1) & ~1)
	        if data[p].ord < _mime_header_parser_trans_keys[_mid]
	          _upper = _mid - 2
	        elsif data[p].ord > _mime_header_parser_trans_keys[_mid+1]
	          _lower = _mid + 2
	        else
	          _trans += ((_mid - _keys) >> 1)
	          _break_match = true
	          break
	        end
	     end # loop
	     break if _break_match
	     _trans += _klen
	  end
	end while false
	_trans = _mime_header_parser_indicies[_trans]
	cs = _mime_header_parser_trans_targs[_trans]
	if _mime_header_parser_trans_actions[_trans] != 0
		_acts = _mime_header_parser_trans_actions[_trans]
		_nacts = _mime_header_parser_actions[_acts]
		_acts += 1
		while _nacts > 0
			_nacts -= 1
			_acts += 1
			case _mime_header_parser_actions[_acts - 1]
when 0 then
# line 12 "src/mime_header.rl"
		begin
 @start_string = p 		end
when 1 then
# line 13 "src/mime_header.rl"
		begin

        if current_value != ''
           metadata[current_header] = current_value
        end
        current_header = data[@start_string...p].pack("c*")
    		end
when 2 then
# line 20 "src/mime_header.rl"
		begin

        current_value = data[@start_string...p].pack("c*")
    		end
when 3 then
# line 23 "src/mime_header.rl"
		begin

        current_value += data[@start_string...p].pack("c*")
    		end
when 4 then
# line 26 "src/mime_header.rl"
		begin

        metadata[current_header] = current_value
    		end
# line 310 "lib/mime_header.rb"
			end # action switch
		end
	end
	if _trigger_goto
		next
	end
	end
	if _goto_level <= _again
	if cs == 0
		_goto_level = _out
		next
	end
	p += 1
	if p != pe
		_goto_level = _resume
		next
	end
	end
	if _goto_level <= _test_eof
	end
	if _goto_level <= _out
		break
	end
	end
	end

# line 64 "src/mime_header.rl"

      metadata
    end

end
